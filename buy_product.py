import psycopg2

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
update_player_inventory = "UPDATE Inventory SET amount = amount + %(amount)s WHERE username =  %(username)s AND product = %(product)s"

def get_connection():
    return psycopg2.connect(
        dbname="test",
        user="postgres",
        password="postgres",
        host="localhost",
        port=5432
    )  # TODO add your values here

def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(update_player_inventory, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username or product name")

            except (psycopg2.errors.CheckViolation, Exception) as e:
                conn.rollback()
                if isinstance(e, psycopg2.errors.CheckViolation):
                    error_message = str(e).lower()
                    if "balance" in error_message:
                        raise Exception("Bad balance")
                    elif "stock" in error_message:
                        raise Exception("Product is out of stock")
                    elif "inventory" in error_message:
                        raise Exception("The inventory maximum is 100 items")
                else:
                    raise e

buy_product('Alice', 'marshmello', 1)
