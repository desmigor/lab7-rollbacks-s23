# Lab7 - Rollbacks and reliability

## 1. Setup

I've setup postgresql using docker. Check out [docker-compose.yaml](https://gitlab.com/desmigor/lab7-rollbacks-s23/-/blob/main/docker-compose.yaml) for the configuration.

## 2. Part 1

In the function `buy_product`, the issue of data inconsistency occurs becaus updating the player's balance and the shop's stock are happening in two different transactions. To resolve this issue, I moved both transactions to occur in  a single transaction. If the transaction fail, we can rollback using  `conn.rollback()`

## 3. Part 2

To handle the player inventory making sure it meets the necessary requirements of the amout not exceeding `100`; I added a `CHECK` on the amount. And before updating the inventory, if this `CHECK` won't be fulfilled, it will rollback.

```sql
CREATE TABLE Inventory (
    PRIMARY KEY (username, product),
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0 AND amount <= 100)
);
```